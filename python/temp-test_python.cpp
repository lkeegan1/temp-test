#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "temp-test/temp-test.hpp"

namespace py = pybind11;

namespace temptest {

PYBIND11_MODULE(temptest, m)
{
  m.doc() = "Python Bindings for My C++ Project";
  m.def("add_one", &add_one, "Increments an integer value");
}

} // namespace temptest
