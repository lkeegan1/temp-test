#include "temp-test/temp-test.hpp"
#include <iostream>

int main(){
  int result = temptest::add_one(1);
  std::cout << "1 + 1 = " << result << std::endl;
}