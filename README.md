# Welcome to My C++ Project

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/lkeegan1/temp-test/main)](https%3A%2F%2Fgit@gitlab.com/lkeegan1/temp-test/-/pipelines)
[![Travis CI](https://img.shields.io/travis/org/lkeegan1/temp-test)](https://travis-ci.org/lkeegan1/temp-test)
[![PyPI Release](https://img.shields.io/pypi/v/temptest.svg)](https://pypi.org/project/temptest)
[![Documentation Status](https://readthedocs.org/projects/temp-test/badge/)](https://temp-test.readthedocs.io/)

# Prerequisites

Building My C++ Project requires the following software installed:

* A C++11-compliant compiler
* CMake `>= 3.9`
* Doxygen (optional, documentation building is skipped if missing)
* Python `>= 3.6` for building Python bindings

# Building My C++ Project

The following sequence of commands builds My C++ Project.
It assumes that your current working directory is the top-level directory
of the freshly cloned repository:

```
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
cmake --build .
```

# Documentation

My C++ Project provides a Sphinx-based documentation, that can
be browsed [online at readthedocs.org](https://temp-test.readthedocs.io).

